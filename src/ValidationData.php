<?php

namespace Itwmw\Validation;

use ArrayAccess;
use Itwmw\Validation\Support\Arr;
use Itwmw\Validation\Support\Str;

class ValidationData
{
    /**
     * 初始化并收集指定属性的数据。
     *
     * @param string $attribute
     * @param  ArrayAccess|array  $masterData
     * @return array
     */
    public static function initializeAndGatherData(string $attribute, $masterData): array
    {
        $data = Arr::dot(static::initializeAttributeOnData($attribute, $masterData));

        return array_merge($data, static::extractValuesForWildcards(
            $masterData,
            $data,
            $attribute
        ));
    }

    /**
     * 收集属性数据的副本，填充任何缺失的属性。
     *
     * @param string $attribute
     * @param  ArrayAccess|array  $masterData
     * @return array
     */
    protected static function initializeAttributeOnData(string $attribute, $masterData): array
    {
        $explicitPath = static::getLeadingExplicitAttributePath($attribute);

        $data = static::extractDataFromPath($explicitPath, $masterData);

        if (! Str::contains($attribute, '*') || Str::endsWith($attribute, '*')) {
            return $data;
        }

        return data_set($data, $attribute, null, true);
    }

    /**
     * 获取给定通配符属性的所有精确属性值。
     *
     * @param  ArrayAccess|array  $masterData
     * @param array $data
     * @param string $attribute
     * @return array
     */
    protected static function extractValuesForWildcards($masterData, array $data, string $attribute): array
    {
        $keys = [];

        $pattern = str_replace('\*', '[^\.]+', preg_quote($attribute));

        foreach ($data as $key => $value) {
            if (preg_match('/^' . $pattern . '/', $key, $matches)) {
                $keys[] = $matches[0];
            }
        }

        $keys = array_unique($keys);

        $data = [];

        foreach ($keys as $key) {
            $data[$key] = Arr::get($masterData, $key);
        }

        return $data;
    }

    /**
     * 根据给定的点符号路径提取数据。
     *
     * 用于提取数据的一个子段，以加快迭代速度。
     *
     * @param string|int $attribute
     * @param ArrayAccess|array $masterData
     * @return array
     */
    public static function extractDataFromPath($attribute, $masterData): array
    {
        $results = [];

        $value = Arr::get($masterData, $attribute, '__missing__');

        if ('__missing__' !== $value) {
            Arr::set($results, $attribute, $value);
        }

        return $results;
    }

    /**
     * 获取属性名的显式部分。
     *
     * E.g. 'foo.bar.*.baz' -> 'foo.bar'
     *
     * 允许我们在某些操作中不旋转通过所有的扁平化数据。
     *
     * @param  string  $attribute
     * @return string
     */
    public static function getLeadingExplicitAttributePath(string $attribute): ?string
    {
        return rtrim(explode('*', $attribute)[0], '.') ?: null;
    }
}
