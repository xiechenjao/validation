<?php

namespace Itwmw\Validation;

use Closure;
use Itwmw\Validation\Support\Interfaces\PresenceVerifierInterface;
use Itwmw\Validation\Support\Str;
use Itwmw\Validation\Support\Translation\ArrayLoader;
use Itwmw\Validation\Support\Translation\Translator;

class Factory
{
    /**
     * 翻译器的实例
     *
     * @var Translator
     */
    protected $translator;

    /**
     * 所有的自定义验证器扩展
     *
     * @var array
     */
    protected $extensions = [];

    /**
     * 所有的自定义隐式验证器扩展
     *
     * @var array
     */
    protected $implicitExtensions = [];

    /**
     * 所有的自定义依赖验证器扩展
     *
     * @var array
     */
    protected $dependentExtensions = [];

    /**
     * 所有的自定义验证器消息替换器
     *
     * @var array
     */
    protected $replacers = [];

    /**
     * 自定义规则的所有后备信息
     *
     * @var array
     */
    protected $fallbackMessages = [];

    /**
     * 存在验证器实现
     *
     * @var PresenceVerifierInterface
     */
    protected $verifier;

    public function __construct(Translator $translator = null, string $local = 'zh_CN')
    {
        if (is_null($translator)) {
            $local = strtolower($local);
            if (!in_array($local, ['zh_cn', 'zh_tw', 'zh_hk', 'en'])) {
                $local = 'zh_cn';
            }
            $langPath = __DIR__ . '/Lang/' . $local . '/validation.php';
            $loader   = new ArrayLoader();
            $loader->addMessages($local, 'validation', require $langPath);
            $translator = new Translator($loader, $local);
        }
        
        $this->translator = $translator;
    }

    public function make(array $data, array $rules, array $messages = [], array $customAttributes = [])
    {
        $validator = new Validator($this->translator, $data, $rules, $messages, $customAttributes);

        if (! is_null($this->verifier)) {
            $validator->setPresenceVerifier($this->verifier);
        }

        $this->addExtensions($validator);

        return $validator;
    }

    /**
     * 为验证器实例添加扩展名。
     *
     * @param  Validator  $validator
     * @return void
     */
    protected function addExtensions(Validator $validator)
    {
        $validator->addExtensions($this->extensions);

        // 接下来，我们将添加隐式扩展，这与所需的
        // 也是被接受的规则，即使属性不在其中，它们也会被运行。
        // 通过实例化提供给验证器实例的数据数组。
        $validator->addImplicitExtensions($this->implicitExtensions);

        $validator->addDependentExtensions($this->dependentExtensions);

        $validator->addReplacers($this->replacers);

        $validator->setFallbackMessages($this->fallbackMessages);
    }

    /**
     * 注册一个自定义验证器扩展。
     *
     * @param  string  $rule
     * @param  Closure|string  $extension
     * @param  string|null  $message
     * @return void
     */
    public function extend(string $rule, $extension, ?string $message = null)
    {
        $this->extensions[$rule] = $extension;

        if ($message) {
            $this->fallbackMessages[Str::snake($rule)] = $message;
        }
    }

    /**
     * 注册一个自定义的隐式验证器扩展。
     *
     * @param  string  $rule
     * @param  Closure|string  $extension
     * @param  string|null  $message
     * @return void
     */
    public function extendImplicit(string $rule, $extension, ?string $message = null)
    {
        $this->implicitExtensions[$rule] = $extension;

        if ($message) {
            $this->fallbackMessages[Str::snake($rule)] = $message;
        }
    }

    /**
     * 注册一个自定义依赖性验证器扩展
     *
     * @param  string  $rule
     * @param Closure|string  $extension
     * @param  string|null  $message
     * @return void
     */
    public function extendDependent(string $rule, $extension, ?string $message = null)
    {
        $this->dependentExtensions[$rule] = $extension;

        if ($message) {
            $this->fallbackMessages[Str::snake($rule)] = $message;
        }
    }

    /**
     * 注册一个自定义验证器信息替换器。
     *
     * @param  string  $rule
     * @param  Closure|string  $replacer
     * @return void
     */
    public function replacer(string $rule, $replacer)
    {
        $this->replacers[$rule] = $replacer;
    }

    /**
     * 设置存在验证器的实现。
     *
     * @param  PresenceVerifierInterface  $presenceVerifier
     * @return void
     */
    public function setPresenceVerifier(PresenceVerifierInterface $presenceVerifier)
    {
        $this->verifier = $presenceVerifier;
    }
}
