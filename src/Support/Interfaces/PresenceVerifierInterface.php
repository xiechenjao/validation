<?php

namespace Itwmw\Validation\Support\Interfaces;

interface PresenceVerifierInterface
{
    /**
     * 设置连接名
     *
     * @param string|null $connection 连接名
     * @return mixed
     */
    public function setConnection(?string $connection);

    /**
     * 指定表名
     *
     * @param string $table 表名
     * @return PresenceVerifierInterface
     */
    public function table(string $table): PresenceVerifierInterface;
    /**
     * 查询数据
     *
     * @param string $column  查询字段
     * @param null $operator  查询表达式
     * @param null $value     查询条件
     * @param string $boolean 查询逻辑
     * @return PresenceVerifierInterface
     */
    public function where(string $column, $operator = null, $value = null, string $boolean = 'and'): PresenceVerifierInterface;

    /**
     * 指定In查询条件
     *
     * @param string $column   查询字段
     * @param mixed  $values   查询条件
     * @param string $boolean  查询逻辑 and or xor
     * @return PresenceVerifierInterface
     */
    public function whereIn(string $column, $values, string $boolean = 'and'): PresenceVerifierInterface;

    /**
     * COUNT查询
     *
     * @param string $columns 字段名
     * @return int
     */
    public function count(string $columns = '*'): int;
}
