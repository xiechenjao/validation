<?php

namespace Itwmw\Validation\Support\Interfaces;

interface Rule
{
    /**
     * 判断验证规则是否通过
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes(string $attribute, $value): bool;

    /**
     * 获取验证的错误消息
     *
     * @return string
     */
    public function message(): ?string;
}
